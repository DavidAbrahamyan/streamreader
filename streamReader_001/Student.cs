﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace streamReader_001
{
    class Student
    {
        public string name;
        public string surname;
        public string phoneNumber;

        public static IEnumerable<Student> CreateRandomStudents(int count)
        {
            Random rnd = new Random();
            for (int i = 0; i < count; i++)
                yield return new Student
                {
                    name = $"A{i}",
                    surname = $"A{i}yan",
                    phoneNumber = rnd.Next(100000, 999999).ToString()
                };
        }
        public Student()
        {
            name = "";
            surname = "";
            phoneNumber = "";
        }

    }
}
