﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace streamReader_001
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"C:\Testdir\Student.txt";
            StreamReader sr = new StreamReader(path);
            int lineCount = File.ReadAllLines(path).Count();
            Student[] stArr = new Student[lineCount];

            for (int i = 0; i < lineCount; i++)
            {
                stArr[i] = new Student();
            }
            int j = 0;
            while (!sr.EndOfStream)
            {
                string st = sr.ReadLine();
                int i = 0;
                while (st[i] != ',')
                {
                    stArr[j].name += st[i];
                    i++;
                }
                i += 2;
                while (st[i] != ',')
                {
                    stArr[j].surname += st[i];
                    i++;
                }
                i += 2;
                while (i != st.Length)
                {
                    stArr[j].phoneNumber += st[i];
                    i++;
                }

                j++;
            }
            PrintStudentInformation(stArr);
            Console.ReadLine();
        }
        public static void PrintStudentInformation(Student[] arrSt)
        {
            foreach (Student st in arrSt)

            {
                Console.WriteLine(st.name + " " + st.surname + " " + st.phoneNumber);
            }
        }
    }
}
